﻿var TodoList = React.createClass({
  getInitialState: function () {
    return {
      haserror: false,
      errormsg: '',
      addTodoListText: null,
      addTodoListTaskText: null,
      todolist: [],
      selectedList: null,
      selectedtasks: [],
      showendedtask: false
    };
  },

  componentDidMount: function() {
    this.loadTodoListService();
  },

  componentWillUnmount: function() {
    this.serverRequest.abort();
  },

  /*
    Load todo list from database.
  */
  loadTodoListService: function() {
    this.serverRequest = $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var firstTodolistElementId = null;

        for (var i=0; i < data.length; i++) {
          if (i == 0) {
            firstTodolistElementId = 'todolist_' + data[i].Id;
          }

          if (data[i].Tasks.length > 0) {
            data[i].Count = data[i].Tasks.length;
            data[i].CountEnded = data[i].Tasks.filter(f => f.Ended).length;
          }
        }

        this.setState({todolist: data});

        // Select first todo list node.
        document.getElementById(firstTodolistElementId).click();
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({errormsg: err.toString()});
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Save new todo list to database.
  */
  addTodoListService: function(text) {
    var todoListItem = {
      Count: 0,
      CountEnded: 0,
      Id: 0,
      Name: text,
      Tasks: []
    };

    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/AddList', //this.props.url,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var item = data.element;
          for (var i=0; i < item.length; i++) {
            if (item[i].Tasks.length > 0) {
              item[i].Count = item[i].Tasks.length;
              item[i].CountEnded = item[i].Tasks.filter(f => f.Ended).length;
            }
          }

          var items = this.state.todolist.slice();
          items.push(item);
          this.setState({todolist: items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Update todo list to database.
  */
  updateTodoListService: function(todoListItem) {
    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/UpdateList', //this.props.url,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var item = data.element;
          var items = this.state.todolist.slice();
          var index = -1;
          for (var i = 0; i < items.length; i++) {
            if (items[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            items.splice(index, 1, item);
            this.state.todolist = items;
          }

          this.setState({todolist:items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Delete todo list from database.
  */
  deleteTodoListService: function(todoListItem) {
    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/DeleteList', //this.props.url,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var item = data.element;
          var items = this.state.todolist.slice();
          var index = -1;
          for (var i = 0; i < items.length; i++) {
            if (items[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            items.splice(index, 1);
          }

          this.setState({todolist:items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Save new task to database.
  */
  addTaskService: function(text) {
    var todolist = this.state.selectedList;

    var taskItem = {
      Id: 0,
      Name: text,
      Ended: false,
      ListId: todolist.Id
    };

    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/AddTask', //this.props.url,
      data: JSON.stringify(taskItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          tasks.push(data.element);
          todolist.Tasks = tasks;
          this.setState({selectedList: todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Update task to database.
  */
  updateTaskService: function(task) {
    var todolist = this.state.selectedList;

    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/UpdateTask', //this.props.url,
      data: JSON.stringify(task),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          var item = data.element;
          var index = -1;
          for (var i = 0; i < tasks.length; i++) {
            if (tasks[i].Id == item.Id) {
              tasks[i] = item;
              break;
            }
          }

          todolist.Tasks = tasks;
          this.setState({selectedList: todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /*
  Delete task from database.
  */
  deleteTaskService: function(task) {
    var todolist = this.state.selectedList;

    this.serverRequest = $.ajax({
      type: 'POST',
      url: 'Home/DeleteTask', //this.props.url,
      data: JSON.stringify(task),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.setState({errormsg: data.errormessage});
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          var item = data.element;
          var index = -1;
          for (var i = 0; i < tasks.length; i++) {
            if (tasks[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            tasks.splice(index, 1);
          }

          todolist.Tasks = tasks;
          this.setState({selectedList:todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  hideerror: function () {
  },

  addlist: function () {
    var text = this.state.addTodoListText;
    this.addTodoListService(text);
    this.setState({addTodoListText: ''});
  },

  addtask: function () {
    var text = this.state.addTodoListTaskText;
    this.addTaskService(text);
    this.setState({addTodoListTaskText: ''});
  },

  checktask: function (task) {
    var todolist = this.state.selectedList;
    var tasks = todolist.Tasks.slice();

    var index = -1;
    for (var i = 0; i < tasks.length; i++) {
      if (tasks[i].Id == task.Id) {
        tasks[i].Ended = !tasks[i].Ended;
        this.updateTaskService(tasks[i]);
        break;
      }
    }
  },

  removetask: function (task) {
    this.deleteTaskService(task);
  },

  showendedtask: function () {
    this.setState({showendedtask: false});
  },

  hideendedtask: function () {
    this.setState({showendedtask: true});
  },

  selectedList: function (listitem) {
    this.setState({selectedList: listitem});
    var tasks = listitem.Tasks == null ? [] : listitem.Tasks;
    this.setState({selectedtasks: tasks});
  },

  removelist: function (listitem) {
    this.deleteTodoListService(listitem);
  },

  isActive:function(selectedList){
    return 'list-group-item listwrapper ' + ((selectedList === this.state.selectedList) ?'active':'');
  },

  addTodoListTextOnInput: function(event) {
    var text = event.target.value == '' ? '' : event.target.value;
    this.setState({addTodoListText: text});
  },

  addTodoListTaskTextOnInput: function(event) {
    var text = event.target.value == '' ? '' : event.target.value;
    this.setState({addTodoListTaskText: text});
  },

  render: function () {
    /*
      Show or hide ended tasks button.
    */
    var taskHandlingButtons;
    if (this.state.showendedtask) {
      taskHandlingButtons = <button onClick={this.showendedtask} type="button" className="btn btn-default showhidetasks">Show ended tasks</button>
    }
    else {
      taskHandlingButtons = <button onClick={this.hideendedtask} type="button" className="btn btn-default showhidetasks">Hide ended tasks</button>
    }

    /*
      Todo list nodes.
    */
    var todolistNodes = this.state.todolist.map(function(listitem) {
      return (
        <a id={'todolist_' + listitem.Id} key={listitem.Id} href="#" onClick={this.selectedList.bind(this, listitem)} className={this.isActive(listitem)}>
          <div className="listitemwrapper">
            <div>{listitem.Name}</div>
            <div className="actionbtn text-danger" onClick={this.removelist.bind(this, listitem)}><i className="glyphicon glyphicon-remove"></i></div>
          </div>
          <span className="badge">{listitem.CountEnded}/{listitem.Count}</span>
        </a>
      )
    }, this);

    /*
      Selected todo list task nodes.
    */
    var showEndedTask = this.state.showendedtask;
    var tasksListNodes = this.state.selectedtasks
      .filter(function(task) {
        return !(showEndedTask && task.Ended);
      }).map(function(task) {
        return (
          <li key={task.Id} className="list-group-item">
          <div className="taskwrapper">
            {(task.Ended
              ? <i className="checkicon glyphicon glyphicon-check" onClick={this.checktask.bind(this, task)}></i>
              : <i className="checkicon glyphicon glyphicon-unchecked" onClick={this.checktask.bind(this, task)}></i>
              )}
            <div>
              {task.Name}
            </div>
            <div className="actionbtn text-danger" onClick={this.removetask.bind(this, task)}><i className="glyphicon glyphicon-remove"></i></div>
          </div>
          </li>
        )
    }, this);

    /*
      Page form node.
    */
    var formNode;
    if (this.state.haserror) {
      formNode = (
        <div className="alert alert-danger alert-dismissible" role="alert">
          <button onClick={this.hideerror} type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div>{this.state.errormsg}</div>
        </div>
      )
    }
    else {
      formNode = (
        <form>
          <div className="leftcontainer borderedboxlight">
            <div className="addcontentlist">
              {(this.state.addTodoListText == null || this.state.addTodoListText == ''
              ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addlist}></i>
              : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addlist}></i>
              )}
              <input type="text" className="form-control" onInput={this.addTodoListTextOnInput} required />
            </div>
            {(this.state.todolist
            ? <div className="list-group">
              {todolistNodes}
            </div>
            : null
            )}
          </div>
          <div className="centercontainer borderedboxlight">
            {(this.state.todolist
            ? <ul className="list-group">
                <li className="list-group-item addcontent">
                  {(this.state.addTodoListTaskText == null || this.state.addTodoListTaskText == ''
                  ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addtask}></i>
                  : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addtask}></i>
                  )}
                  <input type="text" className="form-control" onInput={this.addTodoListTaskTextOnInput} required />
                </li>
              {tasksListNodes}
            </ul>
            : null
            )}
            {taskHandlingButtons}
          </div>
        </form>
      )
    }

    return (
      <div>
        {formNode}
      </div>
      );
  }
});



////////////////////////////////
//// Second version

//var ErrorPanel = React.createClass({
//  getInitialState: function () {
//    return {
//      haserror: (this.props.errormsg == null || this.props.errormsg == '') ? false : true
//    }
//  },

//  componentDidMount: function() {
//  },

//  componentWillUnmount: function() {
//  },

//  hideError: function() {
//    this.setState({haserror: false});
//  },

//  render: function () {
//    var errorMessageNode = null;
//    if (this.state.haserror) {
//      errorMessageNode = (
//        <div className="alert alert-danger alert-dismissible" role="alert">
//          <button onClick={this.hideError} type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
//          <div>{this.props.errormsg}</div>
//        </div>
//      );
//    }

//    return errorMessageNode;
//  }
//});

//var TodoListPanel = React.createClass({
//  getInitialState: function () {
//    return {
//      addTodoListText: '',
//      todolist: [],
//      selectedList: [],
//      selectedtasks: []
//    };
//  },

//  componentDidMount: function() {
//    this.loadTodoListService();
//  },

//  componentWillUnmount: function() {
//  },

//  /*
//  Load todo list from database.
//  */
//  loadTodoListService: function() {
//    $.ajax({
//      url: this.props.url,
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        var firstTodolistElementId = null;

//        for (var i=0; i < data.length; i++) {
//          if (i == 0) {
//            firstTodolistElementId = 'todolist_' + data[i].Id;
//          }

//          if (data[i].Tasks.length > 0) {
//            data[i].Count = data[i].Tasks.length;
//            data[i].CountEnded = data[i].Tasks.filter(f => f.Ended).length;
//          }
//        }

//        this.setState({todolist: data});

//        // Select first todo list node.
//        document.getElementById(firstTodolistElementId).click();
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  /*
//  Save new todo list to database.
//  */
//  addTodoListService: function(text) {
//    var todoListItem = {
//      Count: 0,
//      CountEnded: 0,
//      Id: 0,
//      Name: text,
//      Tasks: []
//    };

//    $.ajax({
//      type: 'POST',
//      url: this.props.addurl,
//      data: JSON.stringify(todoListItem),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var item = data.element;
//          for (var i=0; i < item.length; i++) {
//            if (item[i].Tasks.length > 0) {
//              item[i].Count = item[i].Tasks.length;
//              item[i].CountEnded = item[i].Tasks.filter(f => f.Ended).length;
//            }
//          }

//          var items = this.state.todolist.slice();
//          items.push(item);
//          this.setState({todolist: items});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  /*
//  Update todo list to database.
//  */
//  updateTodoListService: function(todoListItem) {
//    $.ajax({
//      type: 'POST',
//      url: this.props.updateurl,
//      data: JSON.stringify(todoListItem),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var item = data.element;
//          var items = this.state.todolist.slice();
//          var index = -1;
//          for (var i = 0; i < items.length; i++) {
//            if (items[i].Id == item.Id) {
//              index = i;
//            }
//          }

//          if (index > -1) {
//            items.splice(index, 1, item);
//            this.state.todolist = items;
//          }

//          this.setState({todolist:items});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  /*
//  Delete todo list from database.
//  */
//  deleteTodoListService: function(todoListItem) {
//    $.ajax({
//      type: 'POST',
//      url: this.props.deleteurl,
//      data: JSON.stringify(todoListItem),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var item = data.element;
//          var items = this.state.todolist.slice();
//          var index = -1;
//          for (var i = 0; i < items.length; i++) {
//            if (items[i].Id == item.Id) {
//              index = i;
//            }
//          }

//          if (index > -1) {
//            items.splice(index, 1);
//          }

//          this.setState({todolist:items});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  addTodoListTextOnInput: function(event) {
//    var text = event.target.value == '' ? '' : event.target.value;
//    this.setState({addTodoListText: text});
//  },

//  addlist: function () {
//    var text = this.state.addTodoListText;
//    this.addTodoListService(text);
//    this.setState({addTodoListText: ''});
//  },

//  removelist: function (listitem) {
//    this.deleteTodoListService(listitem);
//  },

//  selectedList: function (listitem) {
//    this.setState({selectedList: listitem});
//    var tasks = listitem.Tasks == null ? [] : listitem.Tasks;
//    this.setState({selectedtasks: tasks});
//    this.props.selection(listitem);
//  },

//  isActive:function(selectedList){
//    return 'list-group-item listwrapper ' + ((selectedList === this.state.selectedList) ?'active':'');
//  },

//  render: function () {
//    /*
//    Todo list nodes.
//    */
//    var todolistNodes = this.state.todolist.map(function(listitem) {
//      return (
//        <a id={'todolist_' + listitem.Id} key={listitem.Id} href="#" onClick={this.selectedList.bind(this, listitem)} className={this.isActive(listitem)}>
//          <div className="listitemwrapper">
//            <div>{listitem.Name}</div>
//            <div className="actionbtn text-danger" onClick={this.removelist.bind(this, listitem)}><i className="glyphicon glyphicon-remove"></i></div>
//          </div>
//          <span className="badge">{listitem.CountEnded}/{listitem.Count}</span>
//        </a>
//      )
//    }, this);

//    return (
//    <div className="leftcontainer borderedboxlight">
//      <div className="addcontentlist">
//        {(this.state.addTodoListText == null || this.state.addTodoListText == ''
//        ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addlist}></i>
//        : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addlist}></i>
//        )}
//        <input type="text" className="form-control" value={this.state.addTodoListText} onInput={this.addTodoListTextOnInput} required />
//      </div>
//      {(this.state.todolist
//      ? <div className="list-group">
//        {todolistNodes}
//      </div>
//      : null
//      )}
//    </div>
//    )
//  }
//});

//var TaskPanel = React.createClass({
//  getInitialState: function () {
//    return {
//      showendedtask: false,
//      addTodoListTaskText: null
//    };
//  },

//  componentDidMount: function() {
//  },

//  componentWillUnmount: function() {
//  },

//  /*
//  Save new task to database.
//  */
//  addTaskService: function(text) {
//    var todolist = this.props.selection;

//    var taskItem = {
//      Id: 0,
//      Name: text,
//      Ended: false,
//      ListId: todolist.Id
//    };

//    $.ajax({
//      type: 'POST',
//      url: this.props.addurl,
//      data: JSON.stringify(taskItem),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var tasks = todolist.Tasks.slice();
//          tasks.push(data.element);
//          todolist.Tasks = tasks;
//          this.setState({selectedList: todolist});
//          this.setState({selectedtasks: tasks});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  /*
//  Update task to database.
//  */
//  updateTaskService: function(task) {
//    var todolist = this.props.selection;

//    $.ajax({
//      type: 'POST',
//      url: this.props.updateurl,
//      data: JSON.stringify(task),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var tasks = todolist.Tasks.slice();
//          var item = data.element;
//          var index = -1;
//          for (var i = 0; i < tasks.length; i++) {
//            if (tasks[i].Id == item.Id) {
//              tasks[i] = item;
//              break;
//            }
//          }

//          todolist.Tasks = tasks;
//          this.setState({selectedList: todolist});
//          this.setState({selectedtasks: tasks});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  /*
//  Delete task from database.
//  */
//  deleteTaskService: function(task) {
//    var todolist = this.props.selection;

//    $.ajax({
//      type: 'POST',
//      url: this.props.deleteurl,
//      data: JSON.stringify(task),
//      contentType: "application/json; charset=utf-8",
//      dataType: 'json',
//      cache: false,
//      success: function(data) {
//        if (data.haserror) {
//          this.props.error(data.errormessage);
//        }
//        else
//        {
//          var tasks = todolist.Tasks.slice();
//          var item = data.element;
//          var index = -1;
//          for (var i = 0; i < tasks.length; i++) {
//            if (tasks[i].Id == item.Id) {
//              index = i;
//            }
//          }

//          if (index > -1) {
//            tasks.splice(index, 1);
//          }

//          todolist.Tasks = tasks;
//          this.setState({selectedList:todolist});
//          this.setState({selectedtasks: tasks});
//        }
//      }.bind(this),
//      error: function(xhr, status, err) {
//        this.props.error(data.err.toString());
//        console.error(this.props.url, status, err.toString());
//      }.bind(this)
//    });
//  },

//  addtask: function () {
//    var text = this.state.addTodoListTaskText;
//    this.addTaskService(text);
//    this.setState({addTodoListTaskText: ''});
//  },

//  checktask: function (task) {
//    var todolist = this.state.selectedList;
//    var tasks = todolist.Tasks.slice();

//    var index = -1;
//    for (var i = 0; i < tasks.length; i++) {
//      if (tasks[i].Id == task.Id) {
//        tasks[i].Ended = !tasks[i].Ended;
//        this.updateTaskService(tasks[i]);
//        break;
//      }
//    }
//  },

//  removetask: function (task) {
//    this.deleteTaskService(task);
//  },

//  showendedtask: function () {
//    this.setState({showendedtask: false});
//  },

//  hideendedtask: function () {
//    this.setState({showendedtask: true});
//  },

//  addTodoListTaskTextOnInput: function(event) {
//    var text = event.target.value == '' ? '' : event.target.value;
//    this.setState({addTodoListTaskText: text});
//  },

//  render: function () {
//    /*
//      Show or hide ended tasks button.
//    */
//    var taskHandlingButtons;
//    if (this.state.showendedtask) {
//      taskHandlingButtons = <button onClick={this.showendedtask} type="button" className="btn btn-default showhidetasks">Show ended tasks</button>
//    }
//    else {
//      taskHandlingButtons = <button onClick={this.hideendedtask} type="button" className="btn btn-default showhidetasks">Hide ended tasks</button>
//    }

//    /**
//    * Selected todo list task nodes.
//    * @param {Tasks} selected todo list tasks 
//    * @param {ShowEndedTasks} Hides ended tasks
//    * @return {Task list nodes.
//    */
//    var taskItems = this.props.selection == null ? [] : (this.props.selection.Tasks == null ? [] : this.props.selection.Tasks);
//    var showEndedTask = this.state.showendedtask;
//    var tasksListNodes = taskItems
//      .filter(function(task) {
//        return !(showEndedTask && task.Ended);
//      }).map(function(task) {
//        return (
//          <li key={task.Id} className="list-group-item">
//            <div className="taskwrapper">
//              {(task.Ended
//                ? <i className="checkicon glyphicon glyphicon-check" onClick={this.checktask.bind(this, task)}></i>
//                : <i className="checkicon glyphicon glyphicon-unchecked" onClick={this.checktask.bind(this, task)}></i>
//              )}
//              <div>
//                {task.Name}
//              </div>
//              <div className="actionbtn text-danger" onClick={this.removetask.bind(this, task)}><i className="glyphicon glyphicon-remove"></i></div>
//            </div>
//          </li>
//        )
//      }, this);

//    return (
//      <div className="centercontainer borderedboxlight">
//        {(this.props.selection
//        ? <ul className="list-group">
//            <li className="list-group-item addcontent">
//              {(this.state.addTodoListTaskText == null || this.state.addTodoListTaskText == ''
//              ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addtask}></i>
//              : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addtask}></i>
//              )}
//              <input type="text" className="form-control" value={this.state.addTodoListTaskText} onInput={this.addTodoListTaskTextOnInput} required />
//            </li>
//          {tasksListNodes}
//        </ul>
//        : null
//        )}
//        {taskHandlingButtons}
//      </div>
//      )
//  }
//});

//    var TodoListForm = React.createClass({
//      getInitialState: function () {
//        return {
//          errormsg: '',
//          selection: null
//        }
//      },

//      componentDidMount: function() {
//      },

//      componentWillUnmount: function() {
//      },

//      selection: function(todoList) {
//        this.setState({selection: todoList});
//      },

//      error: function(errorMessage) {
//        this.setState({errormsg: errorMessage});
//      },
      
//      render: function () {
//        return (
//      <div>
//        <ErrorPanel errormsg={this.state.errormsg} />
//        <form>
//          <TodoListPanel url="/Home/GetTodoList" addurl="Home/AddList" updateurl="Home/UpdateList" deleteurl="Home/DeleteList" selection={this.selection} error={this.error} />
//          <TaskPanel addurl="Home/AddTask" updateurl="Home/UpdateTask" deleteurl="Home/DeleteTask" selection={this.state.selection} error={this.error} />
//        </form>
//      </div>
//    )
//      }
//    });