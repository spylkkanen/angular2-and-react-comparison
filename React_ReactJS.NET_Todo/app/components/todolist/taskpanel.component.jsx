﻿/**
* Task panel shows selected todolist tasks.
* @param {props.selection} selected todolist object
* @param {props.addurl} url for saving task to database
* @param {props.updateurl} url for updating task in database
* @param {props.deleteurl} url for deleting task from database
* @param {props.error} callback method for errors. Error message string is passes as parameter
*/
var TaskPanel = React.createClass({
  getInitialState: function () {
    return {
      showendedtask: false,
      addTodoListTaskText: null
    };
  },

  componentDidMount: function() {
  },

  componentWillUnmount: function() {
  },

  /**
  * Save new task to database.
  * @param {text} task name
  */
  addTaskService: function(text) {
    var todolist = this.props.selection;

    var taskItem = {
      Id: 0,
      Name: text,
      Ended: false,
      ListId: todolist.Id
    };

    $.ajax({
      type: 'POST',
      url: this.props.addurl,
      data: JSON.stringify(taskItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          tasks.push(data.element);
          todolist.Tasks = tasks;
          this.setState({selectedList: todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.addurl, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Update task to database.
  * @param {task} task object to be updated
  */
  updateTaskService: function(task) {
    var todolist = this.props.selection;

    $.ajax({
      type: 'POST',
      url: this.props.updateurl,
      data: JSON.stringify(task),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          var item = data.element;
          var index = -1;
          for (var i = 0; i < tasks.length; i++) {
            if (tasks[i].Id == item.Id) {
              tasks[i] = item;
              break;
            }
          }

          todolist.Tasks = tasks;
          this.setState({selectedList: todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.updateurl, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Delete task from database.
  * @param {task} task object to be deleted
  */
  deleteTaskService: function(task) {
    var todolist = this.props.selection;

    $.ajax({
      type: 'POST',
      url: this.props.deleteurl,
      data: JSON.stringify(task),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var tasks = todolist.Tasks.slice();
          var item = data.element;
          var index = -1;
          for (var i = 0; i < tasks.length; i++) {
            if (tasks[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            tasks.splice(index, 1);
          }

          todolist.Tasks = tasks;
          this.setState({selectedList:todolist});
          this.setState({selectedtasks: tasks});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.deleteurl, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Add task.
  */
  addtask: function () {
    var text = this.state.addTodoListTaskText;
    this.addTaskService(text);
    this.setState({addTodoListTaskText: ''});
  },

  /**
  * Mark task as done or undone.
  */
  checktask: function (task) {
    var todolist = this.state.selectedList;
    var tasks = todolist.Tasks.slice();

    var index = -1;
    for (var i = 0; i < tasks.length; i++) {
      if (tasks[i].Id == task.Id) {
        tasks[i].Ended = !tasks[i].Ended;
        this.updateTaskService(tasks[i]);
        break;
      }
    }
  },

  /**
  * Delete task.
  */
  removetask: function (task) {
    this.deleteTaskService(task);
  },

  /**
  * Show all tasks.
  */
  showendedtask: function () {
    this.setState({showendedtask: false});
  },

  /**
  * Hide ended tasks.
  */
  hideendedtask: function () {
    this.setState({showendedtask: true});
  },

  /**
  * Watch task input field text changes.
  */
  addTodoListTaskTextOnInput: function(event) {
    var text = event.target.value == '' ? '' : event.target.value;
    this.setState({addTodoListTaskText: text});
  },

  /**
  * Render component.
  */
  render: function () {
    /*
      Show or hide ended tasks button.
    */
    var taskHandlingButtons;
    if (this.state.showendedtask) {
      taskHandlingButtons = <button onClick={this.showendedtask} type="button" className="btn btn-default showhidetasks">Show ended tasks</button>
    }
    else {
      taskHandlingButtons = <button onClick={this.hideendedtask} type="button" className="btn btn-default showhidetasks">Hide ended tasks</button>
    }

    /**
    * Selected todo list task nodes.
    */
    var taskItems = this.props.selection == null ? [] : (this.props.selection.Tasks == null ? [] : this.props.selection.Tasks);
    var showEndedTask = this.state.showendedtask;
    var tasksListNodes = taskItems
      .filter(function(task) {
        return !(showEndedTask && task.Ended);
      }).map(function(task) {
        return (
          <li key={task.Id} className="list-group-item">
            <div className="taskwrapper">
              {(task.Ended
                ? <i className="checkicon glyphicon glyphicon-check" onClick={this.checktask.bind(this, task)}></i>
                : <i className="checkicon glyphicon glyphicon-unchecked" onClick={this.checktask.bind(this, task)}></i>
              )}
              <div>
                {task.Name}
              </div>
              <div className="actionbtn text-danger" onClick={this.removetask.bind(this, task)}><i className="glyphicon glyphicon-remove"></i></div>
            </div>
          </li>
        )
}, this);

    return (
      <div className="centercontainer borderedboxlight">
        {(this.props.selection
        ? <ul className="list-group">
            <li className="list-group-item addcontent">
              {(this.state.addTodoListTaskText == null || this.state.addTodoListTaskText == ''
              ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addtask}></i>
              : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addtask}></i>
                  )}
    <input type="text" className="form-control" value={this.state.addTodoListTaskText} onInput={this.addTodoListTaskTextOnInput} required />
            </li>
          {tasksListNodes}
        </ul>
            : null
            )}
        {taskHandlingButtons}
      </div>
      )
}
});