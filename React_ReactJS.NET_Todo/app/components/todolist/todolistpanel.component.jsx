﻿/**
* Todolist panel shows all todolists.
* @param {props.selection} callback method for todolist selection. Selected todolist object is passed as parameter
* @param {props.url} url for loading all todolists from database
* @param {props.addurl} url for saving todolist to database
* @param {props.updateurl} url for updating todolist in database
* @param {props.deleteurl} url for deleting todolist from database
* @param {props.error} callback method for errors. Error message string is passes as parameter
*/
var TodoListPanel = React.createClass({
  getInitialState: function () {
    return {
      addTodoListText: '',
      todolist: [],
      selectedList: [],
      selectedtasks: []
    };
  },

  componentDidMount: function() {
    this.loadTodoListService();
  },

  componentWillUnmount: function() {
  },

  /**
  * Load todo list from database.
  */
  loadTodoListService: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var firstTodolistElementId = null;

        for (var i=0; i < data.length; i++) {
          if (i == 0) {
            firstTodolistElementId = 'todolist_' + data[i].Id;
          }

          if (data[i].Tasks.length > 0) {
            data[i].Count = data[i].Tasks.length;
            data[i].CountEnded = data[i].Tasks.filter(f => f.Ended).length;
          }
        }

        this.setState({todolist: data});

        // Select first todo list node.
        document.getElementById(firstTodolistElementId).click();
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Save new todo list to database.
  * @param {text} todolist name
  */
  addTodoListService: function(text) {
    var todoListItem = {
      Count: 0,
      CountEnded: 0,
      Id: 0,
      Name: text,
      Tasks: []
    };

    $.ajax({
      type: 'POST',
      url: this.props.addurl,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var item = data.element;
          for (var i=0; i < item.length; i++) {
            if (item[i].Tasks.length > 0) {
              item[i].Count = item[i].Tasks.length;
              item[i].CountEnded = item[i].Tasks.filter(f => f.Ended).length;
            }
          }

          var items = this.state.todolist.slice();
          items.push(item);
          this.setState({todolist: items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Update todo list to database.
  * @param {todoListItem} todolist object
  */
  updateTodoListService: function(todoListItem) {
    $.ajax({
      type: 'POST',
      url: this.props.updateurl,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var item = data.element;
          var items = this.state.todolist.slice();
          var index = -1;
          for (var i = 0; i < items.length; i++) {
            if (items[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            items.splice(index, 1, item);
            this.state.todolist = items;
          }

          this.setState({todolist:items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Delete todo list from database.
  * @param {todoListItem} todolist object
  */
  deleteTodoListService: function(todoListItem) {
    $.ajax({
      type: 'POST',
      url: this.props.deleteurl,
      data: JSON.stringify(todoListItem),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      cache: false,
      success: function(data) {
        if (data.haserror) {
          this.props.error(data.errormessage);
        }
        else
        {
          var item = data.element;
          var items = this.state.todolist.slice();
          var index = -1;
          for (var i = 0; i < items.length; i++) {
            if (items[i].Id == item.Id) {
              index = i;
            }
          }

          if (index > -1) {
            items.splice(index, 1);
          }

          this.setState({todolist:items});
        }
      }.bind(this),
      error: function(xhr, status, err) {
        this.props.error(err.toString());
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  /**
  * Watch task input field text changes.
  */
  addTodoListTextOnInput: function(event) {
    var text = event.target.value == '' ? '' : event.target.value;
    this.setState({addTodoListText: text});
  },

  /**
  * Watch task input field text changes.
  */
  addlist: function () {
    var text = this.state.addTodoListText;
    this.addTodoListService(text);
    this.setState({addTodoListText: ''});
  },

  /**
  * Delete todolist.
  */
  removelist: function (listitem) {
    this.deleteTodoListService(listitem);
  },

  /**
  * Select todolist.
  * @{listitem} selected todolist
  */
  selectedList: function (listitem) {
    this.setState({selectedList: listitem});
    var tasks = listitem.Tasks == null ? [] : listitem.Tasks;
    this.setState({selectedtasks: tasks});
    this.props.selection(listitem);
  },

  /**
  * Set active style for selected todolist.
  * @{listitem} selected todolist
  */
  isActive:function(selectedList){
    return 'list-group-item listwrapper ' + ((selectedList === this.state.selectedList) ?'active':'');
  },

  /**
  * Render component.
  */
  render: function () {
    /*
    Todo list nodes.
    */
    var todolistNodes = this.state.todolist.map(function(listitem) {
      return (
        <a id={'todolist_' + listitem.Id} key={listitem.Id} href="#" onClick={this.selectedList.bind(this, listitem)} className={this.isActive(listitem)}>
          <div className="listitemwrapper">
            <div>{listitem.Name}</div>
            <div className="actionbtn text-danger" onClick={this.removelist.bind(this, listitem)}><i className="glyphicon glyphicon-remove"></i></div>
          </div>
          <span className="badge">{listitem.CountEnded}/{listitem.Count}</span>
        </a>
      )
    }, this);

    return (
<div className="leftcontainer borderedboxlight">
  <div className="addcontentlist">
    {(this.state.addTodoListText == null || this.state.addTodoListText == ''
    ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addlist}></i>
    : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addlist}></i>
        )}
<input type="text" className="form-control" value={this.state.addTodoListText} onInput={this.addTodoListTextOnInput} required />
  </div>
  {(this.state.todolist
? <div className="list-group">
  {todolistNodes}
</div>
: null
)}
</div>
    )
}
});