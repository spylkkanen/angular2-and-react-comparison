﻿/**
* Error panel shows errors.
* @param {errormsg} Error message to be shown
*/
var ErrorPanel = React.createClass({
  getInitialState: function () {
    return {
      haserror: (this.props.errormsg == null || this.props.errormsg == '') ? false : true
    }
  },

  componentDidMount: function() {
  },

  componentWillUnmount: function() {
  },

  hideError: function() {
    this.setState({haserror: false});
  },

  render: function () {
    var errorMessageNode = null;
    if (this.state.haserror) {
      errorMessageNode = (
        <div className="alert alert-danger alert-dismissible" role="alert">
          <button onClick={this.hideError} type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div>{this.props.errormsg}</div>
        </div>
      );
    }

    return errorMessageNode;
  }
});