﻿/**
* Todolist form.
*/
var TodoListForm = React.createClass({
  getInitialState: function () {
    return {
      errormsg: '',
      selection: null
    }
  },

  componentDidMount: function () {
  },

  componentWillUnmount: function () {
  },

  selection: function (todoList) {
    this.setState({ selection: todoList });
  },

  error: function (errorMessage) {
    this.setState({ errormsg: errorMessage });
  },

  render: function () {
    return (
  <div>
    <ErrorPanel errormsg={this.state.errormsg} />
    <form>
      <TodoListPanel url="/Home/GetTodoList" addurl="Home/AddList" updateurl="Home/UpdateList" deleteurl="Home/DeleteList" selection={this.selection} error={this.error} />
      <TaskPanel addurl="Home/AddTask" updateurl="Home/UpdateTask" deleteurl="Home/DeleteTask" selection={this.state.selection} error={this.error} />
    </form>
  </div>
)
  }
});