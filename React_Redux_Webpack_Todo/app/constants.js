// export const TODO_LIST = {
//   ADD: 'ADD',
//   DELETE: 'DELETE'
// };

export default {
  // API_URL: 'http://localhost:50120/api',            //reads from MVC app
  API_URL: '',
  // CARD: 'card',
  
  SELECT_TODOLIST: 'select todo list',
  TOGGLE_SHOW_ENDEDTASKS: 'toggle show ended tasks',
  TOGGLE_HIDE_ENDEDTASKS: 'toggle hide ended tasks',
  
  FETCH_TODOLISTS: 'fetch todo lists',
  FETCH_TODOLISTS_SUCCESS: 'fetch todo lists success',
  FETCH_TODOLISTS_ERROR: 'fetch todo lists error',

  CREATE_TODOLIST: 'create todo list',
  CREATE_TODOLIST_SUCCESS: 'create todo list success',
  CREATE_TODOLIST_ERROR: 'create todo list error',

  UPDATE_TODOLIST: 'update todo list',
  UPDATE_TODOLIST_SUCCESS: 'update todo list success',
  UPDATE_TODOLIST_ERROR: 'update todo list error',

  DELETE_TODOLIST: 'delete todo list',
  DELETE_TODOLIST_SUCCESS: 'delete todo list success',
  DELETE_TODOLIST_ERROR: 'delete todo list error',

  CREATE_TASK: 'create task',
  CREATE_TASK_SUCCESS: 'create task success',
  CREATE_TASK_ERROR: 'create task error',

  UPDATE_TASK: 'update task',
  UPDATE_TASK_SUCCESS: 'update task success',
  UPDATE_TASK_ERROR: 'update task error',

  DELETE_TASK: 'delete task',
  DELETE_TASK_SUCCESS: 'delete task success',
  DELETE_TASK_ERROR: 'delete task error'
};