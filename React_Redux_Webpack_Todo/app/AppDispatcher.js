// import {Dispatcher} from 'flux';
// let AppDispatcher = new Dispatcher();

// import ListStore from '../stores/ListStore';

// // Register callback with AppDispatcher
// AppDispatcher.register((payload) => {

//   let action = payload.action;
//   let new_item = payload.new_item;
//   let id = payload.id;

//   switch(action) {

//     // Respond to add-item action
//     case 'add-item':
//       ListStore.addItem(new_item);
//       break;
    
//     // Respond to remove-item action
//     case 'remove-item':
//       ListStore.removeItem(id);
//       break;

//     default:
//       return true;
//   }

//   // If action was responded to, emit change event
//   ListStore.emitChange();

//   return true;

// });

// export default AppDispatcher;


import {Dispatcher} from 'flux';
import 'babel-polyfill';

class AppDispatcher extends Dispatcher{
  /**
  * Dispatches three actions for an async operation represented by promise.
  */
  dispatchAsync(promise, types, payload){
    const { request, success, failure } = types;
    this.dispatch({ type: request, payload: Object.assign({}, payload) });
    promise.then(
      response => this.dispatch({
        type: success,
        payload: Object.assign({}, payload, { response })
      }),
      error => this.dispatch({
        type: failure,
        payload: Object.assign({}, payload, { error })
      })
    );
  }
}

export default new AppDispatcher();
