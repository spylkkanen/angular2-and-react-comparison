// import {EventEmitter} from 'events';
// import _ from 'lodash';

// let ListStore = _.extend({}, EventEmitter.prototype, {

//   // Mock default data
//   items: [
//     {
//       name: 'Item 1',
//       id: 0
//     },
//     {
//       name: 'Item 2',
//       id: 1
//     }
//   ],

//   todolists: [],

//   getTodoLists: function() {
//     return this.todolists;
//   },

//   addTodoList: function(todoList) {
//     this.todolists.push(todoList);
//   },

//   // Get all items
//   getItems: function(){
//     return this.items;
//   },

//   // Add item
//   addItem: function(new_item){
//     this.items.push(new_item);
//   },

//   // Remove item
//   removeItem: function(item_id){
    
//     let items = this.items;
    
//     _.remove(items,(item) => {
//       return item_id == item.id;
//     });
    
//     this.items = items;

//   },

//   // Emit Change event
//   emitChange: function(){
//     this.emit('change');
//   },

//   // Add change listener
//   addChangeListener: function(callback){
//     this.on('change', callback);
//   },

//   // Remove change listener
//   removeChangeListener: function(callback) {
//     this.removeListener('change', callback);
//   }

// });

// export default ListStore;


import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';
import update from 'react-addons-update';
import 'babel-polyfill';


class TodolistStore extends ReduceStore {
  //constructor() {
  //  super();

  //  this.state = [];
  //}

   getInitialState() {
     return [];
   }

  getTodolist(id) {
    return this._state.find((todolist)=>todolist.Id === id);
  }

  getTodolistIndex(id) {
    return this._state.findIndex((todolist)=>todolist.Id === id);
  }

  reduce(state, action){
    switch (action.type) {
        case constants.FETCH_TODOLISTS_SUCCESS: {
          let s = action.payload.response;
          for (var i=0; i < s.length; i++) {
            if (s[i].Tasks.length > 0) {
              s[i].Count = s[i].Tasks.length;
              s[i].CountEnded = s[i].Tasks.filter(f => f.Ended).length;
            }
          }
          
          return action.payload.response;
        }
      case constants.CREATE_TODOLIST: {
        let result = update(this.getState(), {
          $push: [action.payload.todolist]
        });

        return result;
      }
      case constants.CREATE_TODOLIST_SUCCESS: {
        let todolistIndex = this.getTodolistIndex(action.payload.todolist.Id);
        let result = update(this.getState(), {
          [todolistIndex]: {
            Id: { $set: action.payload.response.element.Id }
          }
        });

        return result;
      }
      case constants.CREATE_TODOLIST_ERROR: {
        let todolistIndex = this.getTodolistIndex(action.payload.todolist.Id);
        let result = update(this.getState(), {
          $splice:[[todolistIndex, 1]]
        });

        return result;
      }
      // case constants.CREATE_TASK_ERROR: {
      //   let cardIndex = this.getCardIndex(action.payload.cardId);
      //   let taskIndex = this.getState()[cardIndex].tasks.findIndex((task)=>(
      //     task.id == action.payload.task.id
      //   ));
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {
      //         $splice:[[taskIndex, 1]]
      //       }
      //     }
      //   });
      // }

      case constants.DELETE_TODOLIST: {
        let todolistIndex2 = this.getTodolistIndex(action.payload.todolist.Id);
        //let todolistIndex2 = action.payload.todolistIndex;
        let result2 = update(this.getState(), {
          $splice: [[todolistIndex2, 1]]
        });

        return result2;
      }
      case constants.DELETE_TODOLIST_SUCCESS: {
        return state;
      }
      case constants.DELETE_TODOLIST_ERROR: {
        //let todolistIndex = this.getCardIndex(action.payload.todolistId);
        let todolistIndex = action.payload.todolistIndex;

        // this.getState().splice(todolistIndex, 1);
        let result3 = update(this.getState(), {
          $splice: [action.payload.todolistIndex, 0, action.payload]
            // [todolistIndex]: {
            //   tasks: {$splice: [[todolistIndex, 0, action.payload]] }
          // }
        });

        return result3;
      }
      case constants.CREATE_TASK: {
        var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
        // let result = update(this.getState(), {
        //   $push: [action.payload.todolist]
        // });
        var result = update(this.getState(), {          
           [todolistIndex]: {
             Count: { $set: action.payload.todolist.Count + 1 } ,
             Tasks: { $push: [action.payload.task] }
           }
        });
        return result;
      }
      case constants.CREATE_TASK_SUCCESS: {
        var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
        var taskIndex = this.getState()[todolistIndex].Tasks.findIndex((task)=>(
          task.Id === action.payload.task.Id
        ));
        var result = update(this.getState(), {
          [todolistIndex]: {
            Tasks: {
              [taskIndex]: {
                Id: { $set: action.payload.response.element.Id }
              }
            }
          }
        });
        return result;
      }
      case constants.CREATE_TASK_ERROR: {
        var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
        var taskIndex = this.getState()[todolistIndex].Tasks.findIndex((task)=>(
          task.Id === action.payload.task.Id
        ));
        var result = update(this.getState(), {
          [todolistIndex]: {
            Tasks: {
              $splice:[[taskIndex, 1]]
            }
          }
        });
        return result;
      }
      case constants.UPDATE_TASK: {
        var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
        var taskIndex = this.getState()[todolistIndex].Tasks.findIndex((task)=>(
          task.Id === action.payload.task.Id
        ));

        var endedCount = action.payload.task.Ended ? action.payload.todolist.CountEnded + 1 : action.payload.todolist.CountEnded - 1;

        var result = update(this.getState(), {
          [todolistIndex]: {
            CountEnded: { $set: endedCount },
            Tasks: {
              [taskIndex]: {    
                  //$push: [/*action.payload.response.element*/action.payload.task]
                  //Count: { $set: action.payload.todolist.Count + 1},
                  $set: action.payload.task
              }
            }
          }
        });
        return result;
      }
      case constants.UPDATE_TASK_SUCCESS: {
        return state;
      }
      case constants.UPDATE_TASK_ERROR: {
        return state;
      }
      case constants.DELETE_TASK: {
          var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
          var taskIndex = this.getState()[todolistIndex].Tasks.findIndex((task)=>(
            task.Id === action.payload.task.Id
          ));
        var result = update(this.getState(), {
          [todolistIndex]: {
              Tasks: {$splice: [[taskIndex/*action.payload.taskIndex*/, 1]] }
          }
        });
        return result;
      }
      case constants.DELETE_TASK_SUCCESS: {
        return state;
      }
      case constants.DELETE_TASK_ERROR: {
        var todolistIndex = this.getTodolistIndex(/*action.payload.todolistId*/action.payload.todolist.Id);
        var result = update(this.getState(), {
          [todolistIndex]: {
            Tasks: {$splice: [[action.payload.taskIndex, 0, action.payload.task]] }
          }
        });
        return result;
      }
      // case constants.CREATE_CARD:
      //   return update(this.getState(), {$push: [action.payload.card] })

      // case constants.CREATE_CARD_SUCCESS:
      //   cardIndex = this.getCardIndex(action.payload.card.id);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       id: { $set: action.payload.response.id }
      //     }
      //   });

      // case constants.CREATE_CARD_ERROR:
      //   cardIndex = this.getCardIndex(action.payload.card.id);
      //   return update(this.getState(), { $splice:[[cardIndex, 1]]});

      // case constants.TOGGLE_CARD_DETAILS:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       showDetails: { $apply: (currentValue) => (currentValue !== false)? false : true }
      //     }
          //   });

        //case constants.SELECT_TODOLIST:
        //    // todolistIndex = this.getTodolistIndex(action.payload.todoListId);
        //    let todolistIndex = this.getTodolistIndex(action.payload.todolistId);

        //    for(let item of this._state) {
        //        if (item.selected !== undefined) {
        //            item.selected = false;
        //        }
        //    }

        //    let result = update(this.getState(), {
        //       [todolistIndex]: {
        //           selected: { $apply: (currentValue) => (currentValue === undefined || currentValue !== true)? true : false }
        //       }
        //    });

            //return result;

        //return update(this.getState(), {
        //  [todolistIndex]: {
        //      selected: { $apply: (currentValue) => (currentValue === undefined || currentValue !== false)? false : true }
        //  }
        //});

      // case constants.UPDATE_CARD:
      //   cardIndex = this.getCardIndex(action.payload.card.id);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       $set: action.payload.draftCard
      //     }
      //   });

      // case constants.UPDATE_CARD_ERROR:
      //   cardIndex = this.getCardIndex(action.payload.card.id);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       $set: action.payload.card
      //     }
      //   });

      // case constants.UPDATE_CARD_POSITION:
      //   if(action.payload.cardId !== action.payload.afterId) {
      //     cardIndex = this.getCardIndex(action.payload.cardId);
      //     let card = this.getState()[cardIndex]
      //     let afterIndex = this.getCardIndex(action.payload.afterId);
      //     return update(this.getState(), {
      //       $splice: [
      //         [cardIndex, 1],
      //         [afterIndex, 0, card]
      //       ]
      //     });
      //   }

      // case constants.UPDATE_CARD_STATUS:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       status: { $set: action.payload.listId }
      //     }
      //   });

      // case constants.PERSIST_CARD_DRAG_ERROR:
      //   cardIndex = this.getCardIndex(action.payload.cardProps.id);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       status: { $set: action.payload.cardProps.status }
      //     }
      //   });

      // case constants.CREATE_TASK:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {$push: [action.payload.task] }
      //     }
      //   });

      // case constants.CREATE_TASK_SUCCESS:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   taskIndex = this.getState()[cardIndex].tasks.findIndex((task)=>(
      //     task.id == action.payload.task.id
      //   ));
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {
      //         [taskIndex]: {
      //           id: { $set: action.payload.response.id }
      //         }
      //       }
      //     }
      //   });

      // case constants.CREATE_TASK_ERROR:
      //   let cardIndex = this.getCardIndex(action.payload.cardId);
      //   let taskIndex = this.getState()[cardIndex].tasks.findIndex((task)=>(
      //     task.id == action.payload.task.id
      //   ));
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {
      //         $splice:[[taskIndex, 1]]
      //       }
      //     }
      //   });

      // case constants.DELETE_TASK:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {$splice: [[action.payload.taskIndex,1]] }
      //     }
      //   });

      // case constants.DELETE_TASK_ERROR:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {$splice: [[action.payload.taskIndex, 0, action.payload.task]] }
      //     }
      //   });

      // case constants.TOGGLE_TASK:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {
      //         [action.payload.taskIndex]: { done: { $apply: (done) => !done }}
      //       }
      //     }
      //   });

      // case constants.TOGGLE_TASK_ERROR:
      //   cardIndex = this.getCardIndex(action.payload.cardId);
      //   return update(this.getState(), {
      //     [cardIndex]: {
      //       tasks: {
      //         [action.payload.taskIndex]: { done: { $apply: (done) => !done }}
      //       }
      //     }
      //   });

      default:
        return state;
    }
  }
}
export default new TodolistStore(AppDispatcher);
