import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';
import update from 'react-addons-update';
import 'babel-polyfill';


class SessionStore extends ReduceStore {

    getInitialState() {
        return {
            selectedTodolistId: -1,
            showendedtask: false
        };
    }

    // selectTodolist(todolist) {
    //     if (this.getState().selectedTodolistId === -1 && todolist.length/*action.payload.response.length*/ > 0) {
    //         let first = _.head(todolist/*action.payload.response*/);

    //         let result = update(this.getState(), {
    //             selectedTodolistId: { $set: first.Id }
    //         });

    //         return result;
    //     }
    // }

    getSelectedTodolistId(id) {
        return this.getState().selectedTodolistId;
    }

    setSelectedTodolistId(id) {
        if (this.getState().selectedTodolistId === -1 && id !== undefined && id > 0) {
            this.getState().selectedTodolistId = id;
            let result = update(this.getState(), {
                selectedTodolistId: { $set: id }
            });

            return result;
        }
    }

    refreshSelection() {
        let result = update(this.getState(), {
            selectedTodolistId: { $set: this.getState().selectedTodolistId }
        });

        return result;
    }

    reduce(state, action){
        switch (action.type) {
            // case constants.FETCH_TODOLISTS_SUCCESS:
            //     return this.selectTodolist(action.payload.response);
            // case constants.CREATE_TASK:
            // case constants.UPDATE_TASK:
            // case constants.DELETE_TASK:
            //     return this.refreshSelection();
             case constants.SELECT_TODOLIST: {
                 //this.setState({ selectedTodolistId: action.payload.todolistId });
                 let result = update(this.getState(), {
                     selectedTodolistId: { $set: action.payload.todolistId }
                 });
                 return result;

                 //this.getState().selectedTodolistId = action.payload.todolistId;
                 //return this.getState();
             }
            case constants.TOGGLE_SHOW_ENDEDTASKS: {
                let result = update(this.getState(), {
                    showendedtask: { $set: true }
                });
                return result;
            }
            case constants.TOGGLE_HIDE_ENDEDTASKS: {
                let result = update(this.getState(), {
                    showendedtask: { $set: false }
                });
                return result;
            }
            default:
                return state;
        }
    }
}
export default new SessionStore(AppDispatcher);