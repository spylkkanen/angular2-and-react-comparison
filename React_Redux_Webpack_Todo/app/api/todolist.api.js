import 'whatwg-fetch';
import 'babel-polyfill';
import constants from '../constants';

//const API_URL = 'http://kanbanapi.pro-react.com';     //The API_URL has moved into the constants file 
const API_HEADERS = {
  'Content-Type': 'application/json',
  Authorization: 'any-string-you-like'
}

let TodolistAPI = {
  fetchTodolists() {
    return fetch(`${constants.API_URL}/Home/GetTodoList`, {headers:API_HEADERS})
    .then((response) => response.json())
  },

  addTodolist(todolist) {
    return fetch(`${constants.API_URL}/Home/AddList`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(todolist)
    })
    .then((response) => response.json())
  },

  updateTodolist(todolist, draftTodolist) {
    // return fetch(`${constants.API_URL}/Home/UpdateList/${todolist.Id}`, {
    return fetch(`${constants.API_URL}/Home/UpdateList`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(draftTodolist)
    })
  },

  deleteTodolist(todolist/*todolistId*/) {
    // return fetch(`${constants.API_URL}/Home/DeleteList/${todolistId}`, {
    return fetch(`${constants.API_URL}/Home/DeleteList`, {
      method: 'post', //'delete',
      headers: API_HEADERS,
      body: JSON.stringify(todolist)
    })
  },

  addTask(/*todolistId*/todolist, task) {
    // return fetch(`${constants.API_URL}/cards/${cardId}/tasks`, {
    return fetch(`${constants.API_URL}/Home/AddTask`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(task)
    })
    .then((response) => response.json())
  },

  updateTask(/*todolistId*/todolist, task) {
    // return fetch(`${constants.API_URL}/cards/${cardId}/tasks`, {
    return fetch(`${constants.API_URL}/Home/UpdateTask`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(task)
    })
    .then((response) => response.json())
  },

  deleteTask(/*todolistId*/todolist, task) {
    // return fetch(`${constants.API_URL}/cards/${cardId}/tasks/${task.id}`, {
    return fetch(`${constants.API_URL}/Home/DeleteTask`, {
      method: 'post', //'delete',
      headers: API_HEADERS,
      body: JSON.stringify(task)
    })
  },

//   persistCardDrag(cardId, status, index) {
//     return fetch(`${constants.API_URL}/cards/${cardId}`, {
//     	method: 'put',
//     	headers: API_HEADERS,
//     	body: JSON.stringify({status, row_order_position: index})
//     })
//   },

//   addTask(cardId, task) {
//     return fetch(`${constants.API_URL}/cards/${cardId}/tasks`, {
//       method: 'post',
//       headers: API_HEADERS,
//       body: JSON.stringify(task)
//     })
//     .then((response) => response.json())
//   },

//   deleteTask(cardId, task) {
//     return fetch(`${constants.API_URL}/cards/${cardId}/tasks/${task.id}`, {
//       method: 'delete',
//       headers: API_HEADERS
//     })
//   },

//   toggleTask(cardId, task) {
//     return fetch(`${constants.API_URL}/cards/${cardId}/tasks/${task.id}`, {
//     	method: 'put',
//     	headers: API_HEADERS,
//     	body: JSON.stringify({done:!task.done})
//     })
//   }

};

export default TodolistAPI;
