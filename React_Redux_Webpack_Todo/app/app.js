﻿
import React from 'react';
import { render } from 'react-dom';
import TodoListForm from './components/todolist/todolistform.component';

var element = document.getElementById('todolist');
render(<TodoListForm />, element);