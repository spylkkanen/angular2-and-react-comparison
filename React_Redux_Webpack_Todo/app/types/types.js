export class Todolist {
  constructor(id, text, tasks) {
    this.Id = id;
    this.Name = text;
    this.Tasks = tasks === undefined || tasks === null ? []: tasks;
    this.Count = 0;
    this.CountEnded = 0;

    if (this.Tasks.length > 0) {
      this.Count = this.Tasks.length;
      this.CountEnded = this.Tasks.filter(f => f.Ended).length;
    }
  }
}

export class Task {
  constructor(id, todolistId, text, ended) {
    this.Id = id;
    this.ListId = todolistId;
    this.Name = text;
    this.Ended = ended;
  }
}