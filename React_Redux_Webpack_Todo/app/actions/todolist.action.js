import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import TodolistAPI from '../api/Todolist.api';
import {throttle} from '../utils';
// import CardStore from '../stores/CardStore';


let TodoListActionCreators = {
  fetchTodolists() {
    AppDispatcher.dispatchAsync(TodolistAPI.fetchTodolists(), {
      request: constants.FETCH_TODOLISTS,
      success: constants.FETCH_TODOLISTS_SUCCESS,
      failure: constants.FETCH_TODOLISTS_ERROR
    });
  },

  addTodolist(todolist) {
    AppDispatcher.dispatchAsync(TodolistAPI.addTodolist(todolist), {
      request: constants.CREATE_TODOLIST,
      success: constants.CREATE_TODOLIST_SUCCESS,
      failure: constants.CREATE_TODOLIST_ERROR
    }, {todolist});
  },

  updateTodolist(todolist, draftTodolist) {
    AppDispatcher.dispatchAsync(TodolistAPI.updateTodolist(todolist, draftTodolist), {
      request: constants.UPDATE_TODOLIST,
      success: constants.UPDATE_TODOLIST_SUCCESS,
      failure: constants.UPDATE_TODOLIST_ERROR
    }, {todolist, draftTodolist});
  },

  deleteTodolist(todolist, todolistIndex) {
    AppDispatcher.dispatchAsync(TodolistAPI.deleteTodolist(todolist), {
      request: constants.DELETE_TODOLIST,
      success: constants.DELETE_TODOLIST_SUCCESS,
      failure: constants.DELETE_TODOLIST_ERROR
    }, {todolist, todolistIndex});
  },

  // selectTodolist: throttle((todolistId) => {
  //   AppDispatcher.dispatch({
  //     type: constants.SELECT_TODOLIST,
  //     payload: {todolistId}
  //   });
  // },500)

  selectTodolist(todolistId) {
    AppDispatcher.dispatch({
      type: constants.SELECT_TODOLIST,
      payload: {todolistId}
    });
  },

  showEndedTasks() {
    AppDispatcher.dispatch({
      type: constants.TOGGLE_SHOW_ENDEDTASKS,
      payload: true
    });
  },

  hideEndedTasks() {
    AppDispatcher.dispatch({
      type: constants.TOGGLE_HIDE_ENDEDTASKS,
      payload: false
    });
  },

  addTask(todolist/*todolistId*/, task) {
    AppDispatcher.dispatchAsync(TodolistAPI.addTask(todolist/*todolistId*/, task), {
      request: constants.CREATE_TASK,
      success: constants.CREATE_TASK_SUCCESS,
      failure: constants.CREATE_TASK_ERROR
    }, {todolist/*todolistId*/, task});
  },

  updateTask(todolist/*todolistId*/, task) {
    AppDispatcher.dispatchAsync(TodolistAPI.updateTask(todolist, task), {
      request: constants.UPDATE_TASK,
      success: constants.UPDATE_TASK_SUCCESS,
      failure: constants.UPDATE_TASK_ERROR
    }, {todolist, task});
  },

  deleteTask(todolist/*todolistId*/, task, taskIndex) {
    AppDispatcher.dispatchAsync(TodolistAPI.deleteTask(todolist/*todolistId*/, task), {
      request: constants.DELETE_TASK,
      success: constants.DELETE_TASK_SUCCESS,
      failure: constants.DELETE_TASK_ERROR
    }, {todolist/*todolistId*/, task, taskIndex});
  }

//   toggleCardDetails(cardId) {
//     AppDispatcher.dispatch({
//       type: constants.TOGGLE_CARD_DETAILS,
//       payload: {cardId}
//     });
//   },


//   addCard(card) {
//     AppDispatcher.dispatchAsync(KanbanAPI.addCard(card), {
//       request: constants.CREATE_CARD,
//       success: constants.CREATE_CARD_SUCCESS,
//       failure: constants.CREATE_CARD_ERROR
//     }, {card});
//   },

//   updateCard(card, draftCard) {
//     AppDispatcher.dispatchAsync(KanbanAPI.updateCard(card, draftCard), {
//       request: constants.UPDATE_CARD,
//       success: constants.UPDATE_CARD_SUCCESS,
//       failure: constants.UPDATE_CARD_ERROR
//     }, {card, draftCard});
//   },

//   updateCardStatus: throttle((cardId, listId) => {
//     AppDispatcher.dispatch({
//       type: constants.UPDATE_CARD_STATUS,
//       payload: {cardId, listId}
//     });
//   }),

//   updateCardPosition: throttle((cardId , afterId) => {
//     AppDispatcher.dispatch({
//       type: constants.UPDATE_CARD_POSITION,
//       payload: {cardId , afterId}
//     });
//   },500),

//   persistCardDrag(cardProps) {
//     let card = CardStore.getCard(cardProps.id)
//     let cardIndex = CardStore.getCardIndex(cardProps.id)
//     AppDispatcher.dispatchAsync(KanbanAPI.persistCardDrag(card.id, card.status, cardIndex), {
//       request: constants.PERSIST_CARD_DRAG,
//       success: constants.PERSIST_CARD_DRAG_SUCCESS,
//       failure: constants.PERSIST_CARD_DRAG_ERROR
//     }, {cardProps});
//   },

//   createDraft(card) {
//     AppDispatcher.dispatch({
//       type: constants.CREATE_DRAFT,
//       payload: {card}
//     });
//   },

//   updateDraft(field, value) {
//     AppDispatcher.dispatch({
//       type: constants.UPDATE_DRAFT,
//       payload: {field, value}
//     });
//   }

};

export default TodoListActionCreators;
