﻿import React, {Component} from 'react';
import ErrorPanel from './errorpanel.component';
import TodoListPanel from './todolistpanel.component';
import TaskPanel from './taskpanel.component';

/**
* Todolist form.
*/
class TodoListForm extends Component {
  constructor(props){
        super(props);
        this.state = {
          errormsg: '',
          selection: null
        };
    }

  //selection (todoList) {
  //  this.setState({ selection: todoList });
  //}

  error (errorMessage) {
    this.setState({ errormsg: errorMessage });
  }

/*
        <ErrorPanel errormsg={this.state.errormsg} />
        <form>
          <TodoListPanel url="/Home/GetTodoList" addurl="Home/AddList" updateurl="Home/UpdateList" deleteurl="Home/DeleteList" selection={this.selection} error={this.error} />
          <TaskPanel addurl="Home/AddTask" updateurl="Home/UpdateTask" deleteurl="Home/DeleteTask" selection={this.state.selection} error={this.error} />
        </form>
*/
    //<TaskPanel addurl="Home/AddTask" updateurl="Home/UpdateTask" deleteurl="Home/DeleteTask" selection={this.state.selection} error={this.error.bind(this)} />
    //<TodoListPanel url="/Home/GetTodoList" addurl="Home/AddList" updateurl="Home/UpdateList" deleteurl="Home/DeleteList" selection={this.selection.bind(this)} error={this.error.bind(this)} />
  render() {
    return (
      <div>
        <ErrorPanel errormsg={this.state.errormsg} />
        <form>
          <TodoListPanel url="/Home/GetTodoList" addurl="Home/AddList" updateurl="Home/UpdateList" deleteurl="Home/DeleteList" error={this.error.bind(this)} />
          <TaskPanel addurl="Home/AddTask" updateurl="Home/UpdateTask" deleteurl="Home/DeleteTask" error={this.error.bind(this)} />
        </form>
      </div>
    );
  }
}

export default TodoListForm;