﻿import React, {Component, PropTypes} from 'react';
import {Container} from 'flux/utils';
import AppDispatcher from '../../AppDispatcher';
import TodolistStore from '../../stores/Todolist.store';
import SessionStore from '../../stores/Session.store';
import TodoListActionCreators from '../../actions/Todolist.action';
import {Task} from '../../types/types';

/**
* Task panel shows selected todolist tasks.
* @param {props.selection} selected todolist object
* @param {props.addurl} url for saving task to database
* @param {props.updateurl} url for updating task in database
* @param {props.deleteurl} url for deleting task from database
* @param {props.error} callback method for errors. Error message string is passes as parameter
*/
class TaskPanel extends Component {
    constructor(props){
        super(props);

        this.addTaskText = '';
    }

    /**
    * Add task.
    */
    addtask() {
        let todolist = TodolistStore.getTodolist(this.state.session.selectedTodolistId/*this.props.selection*/);
        let task = new Task(0, todolist.Id, this.addTaskText, false);
        TodoListActionCreators.addTask(todolist, task);
    }

    /**
    * Mark task as done or undone.
    */
    checktask(task) {
        var todolist = TodolistStore.getTodolist(this.state.session.selectedTodolistId/*this.props.selection*/);
        var updatedTask = new Task(task.Id, todolist.Id, task.Name, !task.Ended);
        TodoListActionCreators.updateTask(todolist, /*task*/updatedTask);
    }

    /**
    * Delete task.
    */
    removetask(task) {
        let todolist = TodolistStore.getTodolist(this.state.session.selectedTodolistId/*this.props.selection*/);
        TodoListActionCreators.deleteTask(todolist, task);
    }

    /**
    * Show all tasks.
    */
    showendedtask() {
        TodoListActionCreators.showEndedTasks();
    }

    /**
    * Hide ended tasks.
    */
    hideendedtask() {
        TodoListActionCreators.hideEndedTasks();
    }

    /**
    * Watch task input field text changes.
    */
    addTaskTextOnInput(event) {
        var text = event.target.value === '' ? '' : event.target.value;
        this.addTaskText = text;
    }

    /**
    * Render component.
    */
    render() {
        let todolist = TodolistStore.getTodolist(this.state.session.selectedTodolistId/*this.props.selection*/);

        /**
        * Show or hide ended tasks button.
        */
        var taskHandlingButtons;
        if (this.state.session.showendedtask) {
            taskHandlingButtons = <button onClick={this.hideendedtask.bind(this)} type="button" className="btn btn-default showhidetasks">Show ended tasks</button>
        }
        else {
            taskHandlingButtons = <button onClick={this.showendedtask.bind(this)} type="button" className="btn btn-default showhidetasks">Hide ended tasks</button>
        }

        /**
        * Selected todo list task nodes.
        */
        var taskItems = null;
        if (todolist === undefined || todolist === null) {
            taskItems = [];
        }
        else {
            taskItems = todolist.Tasks
        }

        var showEndedTask = this.state.session.showendedtask;

        var tasksListNodes = taskItems
          .filter(function(task) {
              return !(showEndedTask && task.Ended);
          }).map(function(task) {
              return (
                <li key={task.Id} className="list-group-item">
                <div className="taskwrapper">
                  {(task.Ended
                    ? <i className="checkicon glyphicon glyphicon-check" onClick={this.checktask.bind(this, task)}></i>
                    : <i className="checkicon glyphicon glyphicon-unchecked" onClick={this.checktask.bind(this, task)}></i>
                )}
              <div>
              {task.Name}
              </div>
              <div className="actionbtn text-danger" onClick={this.removetask.bind(this, task)}><i className="glyphicon glyphicon-remove"></i></div>
            </div>
          </li>
        )
      }, this);

        return (
        <div className="centercontainer borderedboxlight">
            {(this.state.session.selectedTodolistId !== -1
            ? <ul className="list-group">
                <li className="list-group-item addcontent">
                    {(this.addTodoListTaskText === null || this.addTodoListTaskText === ''
                    ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addtask.bind(this)}></i>
                    : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addtask.bind(this)}></i>
                    )}
                    <input type="text" className="form-control" onInput={this.addTaskTextOnInput.bind(this)} required />
                </li>
                {tasksListNodes}
            </ul>
            : null
            )}
            {taskHandlingButtons}
        </div>
    );
    }
}

TaskPanel.propTypes = {
    addurl: PropTypes.string.isRequired,
    updateurl: PropTypes.string.isRequired,
    deleteurl: PropTypes.string.isRequired
};

TaskPanel.getStores = () => ([TodolistStore, SessionStore]);
TaskPanel.calculateState = (prevState) => ({
   todolist: TodolistStore.getState(),
   session: SessionStore.getState()
});

// export default TaskPanel;
export default Container.create(TaskPanel);