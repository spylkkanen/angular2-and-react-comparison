﻿import React, {Component, PropTypes} from 'react';
import {Container} from 'flux/utils';
import AppDispatcher from '../../AppDispatcher';
import TodolistStore from '../../stores/Todolist.store';
import SessionStore from '../../stores/Session.store';
import TodoListActionCreators from '../../actions/Todolist.action';
import {Todolist} from '../../types/types';

/**
* Todolist panel shows all todolists.
* @param {string} todolist selection. Selected todolist object is passed as parameter.
* @param {string} url for loading all todolists from database.
* @param {string} addurl for saving todolist to database.
* @param {string} updateurl for updating todolist in database.
* @param {string} deleteurl for deleting todolist from database.
* @param {string} callback method for errors. Error message string is passes as parameter.
*/
class TodoListPanel extends Component {
  constructor(props) {
    super(props);

    this.addTodoListText = '';
  }

  componentDidMount() {
    // TodolistStore.addChangeListener(this._onChange.bind(this));
    TodoListActionCreators.fetchTodolists();
  }

  componentWillUnmount() {
    // TodolistStore.removeChangeListener(this._onChange.bind(this));
  }

  // Method to setState based upon Store changes
  _onChange() {
    alert('change');
  }


  /**
  * Watch task input field text changes.
  * @param {object} event from text entry.
  */
  addTodoListTextOnInput(event) {
    var text = event.target.value === '' ? '' : event.target.value;
    this.addTodoListText = text;
  }

  /**
  * Watch task input field text changes.
  */
  addlist() {
    let todolist = new Todolist(0, this.addTodoListText, null);

    TodoListActionCreators.addTodolist(todolist);
  }

  /**
  * Delete todolist.
  * @param {object} listitem selection.
  */
  removelist(todolist) {
    let index = TodolistStore.getTodolistIndex(todolist.Id);
    TodoListActionCreators.deleteTodolist(todolist/*todolist.Id*/, index);
  }

  /**
  * Select todolist.
  * @param {object} listitem selection.
  */
  selectedList(listitem) {
    TodoListActionCreators.selectTodolist(listitem.Id);
    //this.props.selection(listitem.Id);
  }

  /**
  * Set active style for selected todolist.
  * @param {object} selectedList selection.
  * @returns {string} element state class.
  */
  isActive(selectedList) {
      let idx = this.state.todolist.findIndex((todolist)=>todolist.Id === this.state.session.selectedTodolistId && todolist.Id === selectedList.Id);
      let css = idx !== -1 ? 'list-group-item listwrapper active' : 'list-group-item listwrapper';

      return css;
  }

  /**
  * Render component.
  * @returns {string} todo list html markup.
  */
  render() {
    if (SessionStore.getSelectedTodolistId()/*this.state.session.selectedTodolistId*/ === -1 && this.state.todolist.length > 0) {
      var first = _.head(this.state.todolist);
      //TodoListActionCreators.selectTodolist(first.Id);
      SessionStore.setSelectedTodolistId(first.Id);
      //this.props.selection(first.Id);
    }

    /*
    Todo list nodes.
    */
    var todolistNodes = this.state.todolist.map(function(listitem) {
      return (
        <a id={'todolist_' + listitem.Id} key={listitem.Id} href="#" onClick={this.selectedList.bind(this, listitem)} className={this.isActive(listitem)}>
          <div className="listitemwrapper">
            <div>{listitem.Name}</div>
            <div className="actionbtn text-danger" onClick={this.removelist.bind(this, listitem)}><i className="glyphicon glyphicon-remove"></i></div>
          </div>
          <span className="badge">{listitem.CountEnded}/{listitem.Count}</span>
        </a>
      );
    }, this);

    return (
    <div className="leftcontainer borderedboxlight">
      <div className="addcontentlist">
        {(this.state.addTodoListText === null || this.state.addTodoListText === ''
        ? <i className="glyphicon glyphicon-plus actiondisable" onClick={this.addlist.bind(this)}></i>
        : <i className="glyphicon glyphicon-plus actionenable" onClick={this.addlist.bind(this)}></i>
        )}
        <input type="text" className="form-control" onInput={this.addTodoListTextOnInput.bind(this)} required />
      </div>
      {(this.state.todolist
      ? <div className="list-group">
          {todolistNodes}
        </div>
      : null
      )}
    </div>
    );
  }
}

TodoListPanel.propTypes = {
  url: PropTypes.string.isRequired,
  addurl: PropTypes.string.isRequired,
  updateurl: PropTypes.string.isRequired,
  deleteurl: PropTypes.string.isRequired,
  selection: PropTypes.func,
  error: PropTypes.func
};

TodoListPanel.getStores = () => ([TodolistStore, SessionStore]);
TodoListPanel.calculateState = (prevState) => ({
  todolist: TodolistStore.getState(),
  session: SessionStore.getState()
});

// export default TodoListPanel;
export default Container.create(TodoListPanel);