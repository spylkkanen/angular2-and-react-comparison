﻿import React, {Component} from 'react';

/**
* Error panel shows errors.
* @param {errormsg} Error message to be shown
*/
class ErrorPanel extends Component {
  constructor(props){
      super(props);

      this.state = {
        haserror: props.errormsg === null || props.errormsg === '' ? false : true
      };

      this.hideError = this.hideError.bind(this);
  }

  hideError(e) {
    e.preventDefault();
    this.setState({ haserror: false });
  }

  render () {
    let errorMessageNode = null;

    if (this.state.haserror === true) {
      errorMessageNode = (
        <div className="alert alert-danger alert-dismissible" role="alert">
          <button onClick={this.hideError} type="button" className="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div>{this.props.errormsg}</div>
        </div>
      );
    }

    return errorMessageNode;
  }
}

ErrorPanel.propTypes = { errormsg: React.PropTypes.string };
ErrorPanel.defaultProps = { errormsg: null };

export default ErrorPanel;