﻿using System.Web.Mvc;
using System.Linq;

namespace Models.ViewModel
{
  public class Person
  {
    public int Id { get; set; }
    public string Username { get; set; }
  }
}